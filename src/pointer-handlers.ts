import { unreachable } from './unreachable'
import type { PointerEvent } from './interfaces'

export type DOMPointerEvent = Readonly<{
  currentTarget: EventTarget | null
  clientX: number
  clientY: number
  screenX: number
  screenY: number
  shiftKey: boolean
  pointerId: number
  pointerType: string
  button: number
  preventDefault: () => void
  isDefaultPrevented: () => boolean
}>

type EventType = 'down' | 'move' | 'up' | 'cancel' | 'out' | 'leave' | 'enter'

const isEventWithTarget = (
  e: DOMPointerEvent,
): e is DOMPointerEvent & { readonly currentTarget: Element } =>
  e.currentTarget instanceof Element

type EventHandlers = Readonly<{
  onPointerDown: (e: DOMPointerEvent) => void
  onPointerMove: (e: DOMPointerEvent) => void
  onPointerUp: (e: DOMPointerEvent) => void
  onPointerCancel: (e: DOMPointerEvent) => void
  onPointerOut: (e: DOMPointerEvent) => void
  onPointerLeave: (e: DOMPointerEvent) => void
  onPointerEnter: (e: DOMPointerEvent) => void
}>

const isDefaultPrevented = (): false => false
const createCancel = <T>(type: 'cancel', pointerId: number) =>
  ({
    type,
    isDefaultPrevented,
    pointerId,
  }) satisfies PointerEvent<T>
const createMove = <T>(
  type: 'down' | 'move' | 'out',
  pointerId: number,
  button: 'primary' | 'secondary' | null,
  selected: T,
) =>
  ({
    type,
    button,
    selected,
    isDefaultPrevented,
    pointerId,
  }) satisfies PointerEvent<T>
const createUp = <T>(
  type: 'up',
  pointerId: number,
  button: 'primary' | 'secondary' | null,
  selected: T,
) => {
  let defaultPrevented = false
  const preventDefault = () => {
    defaultPrevented = true
  }
  const isDefaultPrevented = () => defaultPrevented
  return {
    type,
    button,
    selected,
    pointerId,
    preventDefault,
    isDefaultPrevented,
  } satisfies PointerEvent<T>
}

export const createHandlers = <T>(
  onEvent: ((e: PointerEvent<T>) => void) | undefined,
  make: (e: DOMPointerEvent, type: EventType) => T,
): EventHandlers => {
  const ev = (type: EventType, e: DOMPointerEvent): PointerEvent<T> => {
    if (!isEventWithTarget(e)) throw new Error("error: can't get position.")
    const button =
      'mouse' !== e.pointerType || 0 === e.button
        ? 'primary'
        : 2 === e.button
          ? 'secondary'
          : null
    const { pointerId } = e
    switch (type) {
      case 'cancel':
        return createCancel('cancel', pointerId)
      case 'down':
        return createMove('down', pointerId, button, make(e, type))
      case 'up':
        return createUp('up', pointerId, button, make(e, type))
      case 'enter':
      case 'move':
        return createMove('move', pointerId, button, make(e, type))
      case 'out':
      case 'leave':
        return createMove('out', pointerId, button, make(e, type))
      default:
        return unreachable(type)
    }
  }
  return {
    onPointerDown: (e: DOMPointerEvent) => onEvent?.(ev('down', e)),
    onPointerMove: (e: DOMPointerEvent) => onEvent?.(ev('move', e)),
    onPointerUp: (e: DOMPointerEvent) => onEvent?.(ev('up', e)),
    onPointerCancel: (e: DOMPointerEvent) => onEvent?.(ev('cancel', e)),
    onPointerOut: (e: DOMPointerEvent) => onEvent?.(ev('out', e)),
    onPointerLeave: (e: DOMPointerEvent) => onEvent?.(ev('leave', e)),
    onPointerEnter: (e: DOMPointerEvent) => onEvent?.(ev('enter', e)),
  } as const
}
