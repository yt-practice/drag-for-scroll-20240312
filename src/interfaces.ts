export type PointerEvent<Selected> =
  | Readonly<{
      type: 'down'
      selected: Selected
      pointerId: number
      button: 'primary' | 'secondary' | null
      isDefaultPrevented: () => false
    }>
  | Readonly<{
      type: 'move'
      selected: Selected
      pointerId: number
      button: 'primary' | 'secondary' | null
      isDefaultPrevented: () => false
    }>
  | Readonly<{
      type: 'out'
      selected: Selected
      pointerId: number
      button: 'primary' | 'secondary' | null
      isDefaultPrevented: () => false
    }>
  | Readonly<{
      type: 'up'
      selected: Selected
      pointerId: number
      button: 'primary' | 'secondary' | null
      preventDefault: () => void
      isDefaultPrevented: () => boolean
    }>
  | Readonly<{
      type: 'cancel'
      pointerId: number
      isDefaultPrevented: () => false
    }>
