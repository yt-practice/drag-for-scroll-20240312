/**
 * ここには到達しない。
 * 適切に switch 文などが書かれているか型レベルでチェックする。
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const unreachable = (_: never): never => {
  throw new Error('unreachable.')
}

/**
 * 要実装なところにとりあえず置いておく用
 */
export const todo = (toImpl?: string): never => {
  if (!toImpl) {
    errorLog('未実装です')
    throw new Error()
  }
  const text = `${toImpl}は未実装です` as const
  errorLog(text)
  throw new Error(text)
}

export const errorLog = (mes: unknown) => {
  if ('function' === typeof alert && alert.toString().includes('[native code]'))
    alert(mes)
  else console.error(mes)
}
