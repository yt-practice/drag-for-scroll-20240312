import type { PointerEvent } from './interfaces'
import { unreachable } from './unreachable'

export const createDragHandler = <Selected>(
  move: (start: Selected | null, end: Selected | null) => void,
  up: (start: Selected, end: Selected) => boolean,
) => {
  const start: Record<number, Selected | null | undefined> = {}
  return (ev: PointerEvent<Selected>) => {
    if (ev.isDefaultPrevented()) return
    switch (ev.type) {
      case 'down': {
        const se = ev.selected
        // if ('primary' !== ev.button) return
        move((start[ev.pointerId] = se), se)
        return
      }
      case 'up': {
        // if ('primary' !== ev.button) return
        const se = ev.selected
        if (up(start[ev.pointerId] || se, se)) ev.preventDefault()
        move((start[ev.pointerId] = null), null)
        return
      }
      case 'move':
        move(start[ev.pointerId] || null, ev.selected)
        return
      case 'out':
        // ignore out ?
        // move(start[ev.pointerId] || null, null)
        return
      case 'cancel':
        move((start[ev.pointerId] = null), null)
        return
      default:
        return unreachable(ev)
    }
  }
}
