import { createRoot } from 'react-dom/client'
import { App } from './app'

const el = document.querySelector('main')
if (el) {
  const root = createRoot(el)
  root.render(<App />)
}
