import { memo, useEffect, useRef, useState } from 'react'
import { BehaviorSubject, Subject } from 'rxjs'
import { createHandlers } from './pointer-handlers'
import { createDragHandler } from './drag-handler'
import type { PointerEvent } from './interfaces'

const rows = Array(300)
  .fill(0)
  .map((_, i) => i)
const cols = Array(20)
  .fill(0)
  .map((_, i) => i)

const BigContent = ({ pre }: { pre: string }) => (
  <div
    style={{
      display: 'grid',
      gridTemplateRows: rows.map(() => '1fr').join(' '),
      gridTemplateColumns: cols.map(() => '1fr').join(' '),
    }}
  >
    {rows.flatMap(row =>
      cols.map(col => (
        <div
          key={`${row}/${col}`}
          style={{
            display: 'inline-block',
            background: (row + col) % 2 ? '#eee' : '#ddd',
            gridRow: `${1 + row} / ${2 + row}`,
            gridColumn: `${1 + col} / ${2 + col}`,
            padding: '.5em',
          }}
        >
          {pre}({1 + row}/{1 + col})
        </div>
      )),
    )}
  </div>
)

interface HTMLElement {
  scrollLeft: number
  scrollTop: number
  clientWidth: number
  clientHeight: number
  querySelector: (e: string) => HTMLElement | null
  dataset: Record<string, string | undefined>
}

type Selected = {
  x: number
  y: number
  d: () => HTMLElement | null
}

const scrollManager = (divRef: () => HTMLElement | null) => {
  const ob = new Subject<PointerEvent<Selected>>()
  const draging = new BehaviorSubject(false)
  const h = createHandlers<Selected>(
    e => ob.next(e),
    e => {
      const div = divRef()?.querySelector('.hoge:hover') || null
      const idx = div?.dataset.idx
      return {
        x: e.screenX,
        y: e.screenY,
        d: () =>
          (idx && divRef()?.querySelector(`.hoge[data-idx="${idx}"]`)) || null,
      }
    },
  )
  const start = () => {
    let prev = null as null | {
      x: number
      y: number
      d: () => HTMLElement | null
    }
    const move = (e: Selected, up: boolean) => {
      const current = { x: e.x, y: e.y }
      if (prev) {
        const div = prev.d()
        if (div) {
          div.scrollLeft += current.x - prev.x
          div.scrollTop += current.y - prev.y
        }
      }
      prev = up ? null : { ...current, d: prev?.d || e.d }
    }
    const h = createDragHandler<Selected>(
      (s, e) => {
        draging.next(!!(s && e))
        if (s && e) move(e, false)
      },
      (s, e) => {
        if (s && e) move(e, true)
        return false
      },
    )
    const s = ob.subscribe(e => h(e))
    return () => s.unsubscribe()
  }
  return { h, start, draging }
}

const Pain = ({ idx, pre }: { idx: number; pre: string }) => (
  <div
    style={{
      gridColumn: `${idx} / ${1 + idx}`,
      overflow: 'scroll',
    }}
    className="hoge"
    data-idx={idx}
  >
    <BigContent pre={pre} />
  </div>
)

const Pains = memo(() => (
  <>
    <Pain pre="hoge" idx={1} />
    <Pain pre="fuga" idx={2} />
  </>
))

// main
export const App = () => {
  const divRef = useRef<HTMLDivElement>(null)
  const sm = (useRef<ReturnType<typeof scrollManager> | null>(null).current ||=
    scrollManager(() => divRef.current))
  useEffect(() => sm.start(), [])
  const [draging, setDraging] = useState(sm.draging.getValue())
  useEffect(() => {
    const s = sm.draging.subscribe(s => setDraging(s))
    return () => s.unsubscribe()
  }, [])
  return (
    <div
      style={{
        position: 'absolute',
        inset: 0,
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
        userSelect: 'none',
        ...(draging ? { cursor: 'grab' } : {}),
      }}
      ref={divRef}
      {...sm.h}
      onContextMenu={e => e.preventDefault()}
      key="wrap"
    >
      <Pains />
    </div>
  )
}
